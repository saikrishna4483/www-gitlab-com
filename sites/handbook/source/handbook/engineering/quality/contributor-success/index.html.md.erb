---
layout: handbook-page-toc
title: "Contributor Success Team"
description: "Contributor Success Team"
---

## On this page

{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .toc-list-icons .hidden-md .hidden-lg}

{::options parse_block_html="true" /}

- - -

## Strategy

This team directly follows the strategy outlined in our [open source growth strategy](../../open-source/growth-strategy.html).

## Mission

The goal of the team is to increase the technical improvements and efficiency of our contribution process to sustain our ambition of [1000+ contributors with merged MRs per month to GitLab](/company/strategy/#2-build-on-our-open-core-strength).

### FY23 Direction

In FY23 we will be focused on growing the number of unique new monthly
contributors, reducing OCMA and increasing MRARR. This will increase development
velocity without requiring additional engineering resources. Parallel to this
we'll increase observability of community contributed value through improving
the business intelligence around it. This will allow us to create some
predictability through foreshadowing. These efforts are cross-functional and
require working together with Marketing and Product Development.

This accounts for 70 - 80% of the workload. The remaining 20 - 30% is ad-hoc
work. The ad-hoc work is eclectic and ranges from supporting customers on
contributions, supporting various open source initiatives and supporting the
Engineering Productivity team.

#### Unique New Monthly Contributors

1. Minimize reliance on human interaction
1. Reduce volatility through introducing automations that drive contributions
   forward automatically
1. Capitalize on untapped potential - MRs that are active but not merged at the
   end of every single MR
1. Invest into attracting more new contributors

#### Community Contributed Value Observability

1. Introduce measurement points in various places in the contribution pipeline
1. Collect objective and subjective feedback from contributors about the process
1. Create insight into actual community contribution pipeline size
1. Standardize contribution MRARR valuation
1. Categorize contribution and measure value per type

#### Community Metrics Foreshadowing

1. Predict community metric trends
1. Empower teams to react to negative trends before they actualize
1. Define ambitious targets for FY24

#### OCMA

1. Minimize reliance on human factors that contribute to a large standard deviation
1. Support Engineering Productivity in driving OCMA down

## Team Responsibilities

* Carry out Engineering open source outreach efforts.
* Improve GitLab's Contribution Efficiency and Merge Request Coaching process.
* Engineering representative that can ride along with the evangelist and program manager.
* Organize community contributors into [community teams](team-structure.html) and ensure their success.
* Provide guidance to community contributors on technical and non-technical aspects.
* Track [contribution](/community/contribute/) delivery of the Community Contributors and Cohorts.
* Improve community recognition system and awards in collaboration with Community Relations team.
* Nominate impactful community contributors and contributions for recognition.
* Contribute as an MR Coach in one or more MR Coach specialties.
* Provide guidance and coaching to team members on technical contributions, product architecture, and other areas.
* Be a point of escalation for community contributors and identify GitLab DRIs to resolve blockers
* Participate in GitLab's overall open source outreach events and processes.
* Collaborate closely with our Marketing counterparts and Core team.

## OKRs

Every quarter, the team commits to [Objectives and Key Results (OKRs)](/company/okrs/). The below shows current and previous quarter OKRs, it updates automatically as the quarter progresses.

### Current quarter

<iframe src="https://app.ally.io/public/TQRmKHpOC7NoZb7" class="dashboard-embed" height="1600" width="100%" style="border:none;"> </iframe>

### Previous quarter

<iframe src="https://app.ally.io/public/e0izWt13OqmGHuO" class="dashboard-embed" height="1600" width="100%" style="border:none;"> </iframe>

## Performance Indicators

We have the following Performance Indicators

#### [Unique Wider Community Contributors per Month](/handbook/engineering/quality/performance-indicators/#unique-wider-community-contributors-per-month)

- Target: increase to be greater than 200 per month by FY23Q4
- Activities:
  - Partnership with Community Relations and Technical Marketing team.
  - Hold community office hours
  - Hold hackathons
  - Allow running of QA tests from forks
  - Shorten the CI runtime for community contributions (in forks)

#### [Open Community MR Age](/handbook/engineering/quality/performance-indicators/#open-community-mr-age)

- Target: decrease to lower than 30 days by FY23Q4
- Activities:
  - Shorten CI time
  - Improve Community Contribution automation
  - Enable running QA tests on forks
  - Increase number of coaches
  - Partner with Engineering Productivity to provide feedback to improve contribution tooling (currently GDK).      

#### [MRARR](/handbook/engineering/quality/performance-indicators/#mrarr)

- Target: increase to 400M MR$ by FY23Q4
- Activities:
  - Reach out to top tier enterprise customers
  - Help take-on inactive customer contribution to completion & merge
  - Partner with TAMs to enlist and facilitate contribution from customers
  - Launch contribution materials targeting large enterprises
  - Partner with Community relations team (David P)
  - Maintain a list of known contributors with a mapping to their accounts and the accounts ARR contribution as input to this KPI  

#### [Community Coaches per Month](/handbook/engineering/quality/performance-indicators/#community-mr-coaches-per-month)

- Target: increase to be greater than 50 per month by FY23Q3
- Activities:
  - Work with Development Department (Christopher L, VP of Development) for volunteers.
  - Work with UX Department (Christie L, VP of UX) Christie for volunteers.
  - Refresh MR coaches as “Community coaches” so non-code review work can be encouraged (design, etc)
  - Launch training materials for coaches

#### [Community Contribution MRs as Features per Month](/handbook/engineering/quality/performance-indicators/#community-contribution-mrs-added-as-features-per-month)

- Target: increase to 30% by FY23Q2
- Activities:
  - Encourage features at Community relations hackathons.
  - Published list of feature issues with Marketing team.

## Sisense SQL snippets

### `product_community_contribution_mrs`

Merge requests with ~"Community contribution" label, opened in
[projects that are part of the product](/handbook/engineering/metrics/#projects-that-are-part-of-the-product).

The results include MR type, MR age, MR labels, MR stage, MR group, MR section.

#### Finding the absolute Merge Request URL

It includes 4 joins of the `gitlab_dotcom_groups_xf` table. This is because it is hard to understand the entire hierarchy from the namespace itself. A namespace can be nested within a group of a group of a group, or deeper. So it's important to find the entire hierarchy so we can re-assemble the full URL path of this particular Merge Request.

The end result could be, in the maximum form: `https://gitlab.com/group4/group3/group2/group1/project_path/merge_requests/merge_request_id`

#### Categorization of MRs
Given that not all MRs have type labels yet, we try to guess the type based on existing labels. In the snippet you can clearly see the 3 types being represented (feature, maintenance, bug).

#### Last Active Date
The snippet also goes to great lengths to find out when a merge request was last active. Given that the state could either be open, merged or closed, it has a different logic for each state to understand what the last active date was. Using this date, it can then be used to understand the amount of days the merge request was or is open for, also known as Merge Request Age.

#### Excluding users from the report

There are certain authors or MRs that are excluded from the reports as they belong to the GitLab organization and should not be accounted for when looking at our Community MR metrics. If you do see authors that should be excluded, they should be added to this snippet so they can be excluded from the metrics in the future. Example users are the GitLab Bot, Release Bot, Reviewer/Recommender Bot, etc...

### `community_contributions_base`

Similar to `product_community_contribution_mrs`, but the results include MR lifecycle data (time to triage, time to first review, projected days to merge etc).

## Team Members

<%= direct_team(manager_role: 'Director, Contributor Success') %>

##  How to reach us

### Chat with us

Contributor Success team uses these Slack channels:

- [#g_contributor-success](https://gitlab.slack.com/archives/C02R0NE6P6C): Our team slack channel
- [#wg_contribution-efficiency](https://gitlab.slack.com/archives/C0223D98HHC): For our standup with the Community Relations team
- [#mrarr-wins](https://gitlab.slack.com/archives/C01NTMN6U0P): For announcing new MRARR merge wins
- [#quality](https://gitlab.slack.com/archives/C3JJET4Q6): For discussions intra-department in Quality

### File an issue

File an issue to work with us: [in our task project](https://gitlab.com/gitlab-com/quality/contributor-success) 

## Project Management

* Our [team project](https://gitlab.com/gitlab-com/quality/contributor-success) is our single source of truth for all tasks & backlog.
* Epics that contain cross-functional work across multiple departments can be created at the `gitlab-com` [level](https://gitlab.com/groups/gitlab-com/-/epics?state=opened&page=1&sort=start_date_desc).

This team has the following immediate work items planned.

* [Contribution Efficiency Improvements Epic](https://gitlab.com/groups/gitlab-com/-/epics/1619)
* [Increasing MRARR through internal partnerships](https://gitlab.com/groups/gitlab-com/-/epics/1225)

## Automations owned by the team

Triage automations are handled by the [Triage Ops project](https://gitlab.com/gitlab-org/quality/triage-ops/). Please see the project documentation on instructions how to create new or edit existing automations.

### Reports

1. [Weekly community contribution report](https://gitlab.com/gitlab-org/quality/triage-ops/-/blob/master/policies/groups/gitlab-org/quality/community-contribution-mr-report.yml)
    * A weekly report on community MRs that are actionable
1. [Newly created Community contribution merge requests triage](/handbook/engineering/quality/triage-operations/#newly-created-community-contribution-merge-requests-requiring-first-triage)
1. [Community merge requests requiring attention](/handbook/engineering/quality/triage-operations/#community-merge-requests-requiring-attention)
    * You can find a list of steps on how to nudge them in the [report template](https://gitlab.com/gitlab-org/quality/triage-ops/-/blob/master/policies/groups/gitlab-org/quality/community-contribution-mr-report.yml#L10). 

### Reactive processors

1. [Community contribution thank you note](https://gitlab.com/gitlab-org/quality/triage-ops/-/blob/master/triage/processor/community/thank_contribution.rb)

   - Conditions:
      - MR was opened
      - Author is from the wider community under the `gitlab-org` group, or the `gitlab-com/www-gitlab-com` project
   - Actions:
      - Posts a "Thank you" note
      - Adds `~"Community contribution"` and `~"workflow::in dev"`
      - Assigns the MR to its author
1. [Automated review request](https://gitlab.com/gitlab-org/quality/triage-ops/-/blob/master/triage/processor/community/automated_review_request_generic.rb)

   - Conditions:
      - `~"workflow::ready for review"` was added
      - Author is from the wider community under the `gitlab-org` group
   - Actions:
      - Removes the "Draft" status if present
   - Rate limiting: once per MR per day
1. [Automated review request for doc contributions](https://gitlab.com/gitlab-org/quality/triage-ops/-/blob/master/triage/processor/community/automated_review_request_doc.rb)

   - Conditions:
      - `~"workflow::ready for review"` was added
      - Author is from the wider community under the `gitlab-org` group
      - MR has documentation changes
      - No existing note asking for documentation review
   - Actions:
      - Asks a relevant Technical Writer (based on the changes' mapped from the `CODEOWNERS` file) to review
1. [Automated review request for UX contributions](https://gitlab.com/gitlab-org/quality/triage-ops/-/blob/master/triage/processor/community/automated_review_request_ux.rb)

   - Conditions:
      - `~"workflow::ready for review"` was added
      - Author is from the wider community under the `gitlab-org` group
      - `~UX` is set
      - No existing note asking for UX review
   - Actions:
      - Posts a Slack message in the `#ux-community-contributions` Slack channel (internal) to ask a UX reviewer to review non-draft MRs
      - Posts a note to let the author know about the Slack ping
1. [Reactive `help` command](https://gitlab.com/gitlab-org/quality/triage-ops/-/blob/master/triage/processor/community/command_mr_help.rb)

   - Conditions:
      - MR note starts with `@gitlab-bot help`
      - Note author is from the wider community under the `gitlab-org` group
   - Actions:
      - Pings and assigns (as reviewer) a random MR coach for help
   - Rate limiting: once per MR per day
1. [Reactive `ready` command](https://gitlab.com/gitlab-org/quality/triage-ops/-/blob/master/triage/processor/community/command_mr_request_review.rb) (deprecated)

   - Conditions:
      - MR note starts with `@gitlab-bot ready`, `@gitlab-bot review`, or `@gitlab-bot request_review`
      - Note author is from the wider community under the `gitlab-org` group
   - Actions:
      - Adds `~"workflow::ready for review"` to the MR
   - Rate limiting: once per MR per day
1. [Reactive `label` command](https://gitlab.com/gitlab-org/quality/triage-ops/-/blob/master/triage/processor/community/command_mr_label.rb)

   - Conditions:
      - MR note starts with `@gitlab-bot label ~"label-name"` where `label-name` matches `group::*`, `type::*` or is `~"workflow::in dev"` or `~"workflow::ready for review"`
      - Note author is from the wider community under the `gitlab-org` group
      - Note author is the same as the MR author
   - Actions:
      - Posts a quick action to add the requested label
   - Rate limiting: 60 times per author per hour
1. [Idle/Stale label remover](https://gitlab.com/gitlab-org/quality/triage-ops/-/blob/master/triage/processor/community/remove_idle_labels_on_activity.rb)

   - Conditions:
      - MR note
      - Note author is from the wider community under the `gitlab-org` group
      - `~idle` or `~stale` are set, or author pushed changes to the MR
   - Actions:
      - Removes `~idle` and `~stale`
1. [Code Review Experience Feedback](https://gitlab.com/gitlab-org/quality/triage-ops/-/blob/master/triage/processor/community/code_review_experience_feedback.rb)

   - Conditions:
      - MR was merged or closed
      - MR author is from the wider community under the `gitlab-org` or `gitlab-com` group
      - No existing note asking for feedback
   - Actions:
      - Posts a note to ask MR author about their contributing experience
1. [Reactive `feedback` command](https://gitlab.com/gitlab-org/quality/triage-ops/-/blob/master/triage/processor/community/command_mr_feedback.rb)

   - Conditions:
      - MR note starts with `@gitlab-bot feedback`
      - Note author is from the wider community under the `gitlab-org` group
      - Note author is the same as the MR author
   - Actions:
      - Posts a Slack message in the `#mr-feedback` Slack channel (internal) pointing to the contributor feedback note
1. [Hackathon labeler](https://gitlab.com/gitlab-org/quality/triage-ops/-/blob/master/triage/processor/community/hackathon_label.rb)

   - Conditions:
      - MR was opened or updated during the Hackathon dates
      - MR author is from the wider community under the `gitlab-org` or `gitlab-com` group
      - No existing note mentioning the Hackathon
   - Actions:
      - Posts a note mentioning the Hackathon
      - Adds the `~Hackathon` label

## Working with community contributions

### Community contribution labels

- The `Community contribution` label is automatically applied by the [GitLab Bot](https://gitlab.com/gitlab-bot) to MRs submitted by wider community members. 
  * You can see the list of MRs in [`gitLab-org` list of merge requests](https://gitlab.com/groups/gitlab-org/-/merge_requests?label_name[]=Community+contribution).
  * [Learn more about the cadence and conditions for this automation](/handbook/engineering/quality/triage-operations/#community-contributions).
- The `1st contribution` label is added to first-time contributions. Every time a contributor is opening a merge request under the `gitlab-org` namespace for the first time, the label `1st contribution` is automatically applied to the merge request.
  - You can see the list of MRs in [`gitlab-org` list of merge requests](https://gitlab.com/groups/gitlab-org/-/merge_requests?label_name%5B%5D=1st+contribution).
  - [First-time contributors](/handbook/marketing/community-relations/code-contributor-program/#first-time-contributors) are also awarded a gift as our way to say thanks.

### Community merge request workflow automations

Community merge requests are MRs opened by a person that's not a member of the [`gitlab-org`](https://gitlab.com/-/ide/project/gitlab-org) nor [`gitlab-com`](https://gitlab.com/-/ide/project/gitlab-com) groups.

```mermaid
graph LR
    classDef triageOpsClass fill:#FC6D26,stroke:#333,stroke-width:3px;

    MR_INITIAL(["Wider Community Merge request<br />(author is not a member of `gitlab-org`)"])
    MR_OPENED[MR is opened]
    MR_UPDATED[MR is updated]
    MR_MERGED[MR is merged]
    MR_CLOSED[MR is closed]
    MR_AUTHOR_NOTE[MR author posts a note]
    ANYONE_NOTE[Anyone posts a note]
    AUTOMATED_THANK(["1. 'Thank you' note posted<br/>2. Adds ~'Community contribution'<br />3. Adds ~'workflow::in dev'<br />4. MR assigned to author"])
    WORKFLOW_READY_FOR_REVIEW_LABEL{"~'workflow::ready for review'<br />was added?"}
    AUTOMATED_REVIEWER_REQUEST_GENERIC(["Remove Draft status if present"])
    AUTOMATED_REVIEW_DOC{"Does the MR touches<br/>documentation files?"}
    AUTOMATED_REVIEWER_REQUEST_DOC(["A technical writer<br />is asked to review"])
    AUTOMATED_REVIEW_UX{"Does the MR has<br />the ~UX label?"}
    AUTOMATED_REVIEWER_REQUEST_UX(["A message is posted in the<br />`#ux-community-contributions`<br />Slack channel"])
    AUTOMATED_FEEDBACK_REQUEST(["Post a note asking<br />for feedback"])
    AUTOMATED_HACKATHON_LABEL{Is a Hackathon currently running?}
    AUTOMATED_HACKATHON_LABEL_ADDITION(["The ~Hackathon label is added"])
    WHAT_AUTHOR_NOTE{What note is it?}
    WHAT_ANYONE_NOTE{What note is it?}
    
    AUTOMATED_LABEL_COMMAND_REPLY(["Quick action note posted<br />to add the requested label"])
    AUTOMATED_HELP_COMMAND_REPLY(["Ping and assign<br />an MR coach"])
    AUTOMATED_REVIEW_COMMAND_REPLY(["Add ~'workflow::ready for review'"])
    AUTOMATED_FEEDBACK_COMMAND_REPLY(["A message is posted in the<br />`#mr-feedback` Slack channel"])

    MR_INITIAL -.-> MR_OPENED & MR_UPDATED & MR_MERGED & MR_CLOSED & MR_AUTHOR_NOTE & ANYONE_NOTE

    MR_OPENED ----> AUTOMATED_THANK
    MR_UPDATED -.-> WORKFLOW_READY_FOR_REVIEW_LABEL
    MR_OPENED & MR_UPDATED -.-> AUTOMATED_HACKATHON_LABEL
    MR_MERGED & MR_CLOSED ----> AUTOMATED_FEEDBACK_REQUEST
    MR_AUTHOR_NOTE -.-> WHAT_AUTHOR_NOTE
    ANYONE_NOTE -.-> WHAT_ANYONE_NOTE

    WORKFLOW_READY_FOR_REVIEW_LABEL ---> |Yes| AUTOMATED_REVIEWER_REQUEST_GENERIC
    WORKFLOW_READY_FOR_REVIEW_LABEL -.-> |Yes| AUTOMATED_REVIEW_DOC & AUTOMATED_REVIEW_UX
    AUTOMATED_REVIEW_DOC -->|Yes| AUTOMATED_REVIEWER_REQUEST_DOC
    AUTOMATED_REVIEW_UX -->|Yes| AUTOMATED_REVIEWER_REQUEST_UX
    AUTOMATED_HACKATHON_LABEL --->|Yes| AUTOMATED_HACKATHON_LABEL_ADDITION

    WHAT_AUTHOR_NOTE --->|"@gitlab-bot label ..."| AUTOMATED_LABEL_COMMAND_REPLY
    WHAT_AUTHOR_NOTE --->|"@gitlab-bot feedback"| AUTOMATED_FEEDBACK_COMMAND_REPLY

    WHAT_ANYONE_NOTE --->|"@gitlab-bot help"| AUTOMATED_HELP_COMMAND_REPLY
    WHAT_ANYONE_NOTE --->|"@gitlab-bot ready"| AUTOMATED_REVIEW_COMMAND_REPLY

    class AUTOMATED_THANK,AUTOMATED_LABEL_COMMAND_REPLY,AUTOMATED_HELP_COMMAND_REPLY triageOpsClass;
    class AUTOMATED_REVIEW_COMMAND_REPLY,AUTOMATED_FEEDBACK_REQUEST,AUTOMATED_REVIEW_DOC triageOpsClass;
    class AUTOMATED_REVIEW_UX,AUTOMATED_REVIEWER_REQUEST_DOC,AUTOMATED_REVIEWER_REQUEST_UX triageOpsClass;
    class AUTOMATED_FEEDBACK_COMMAND_REPLY,AUTOMATED_HACKATHON_LABEL triageOpsClass;
    class AUTOMATED_HACKATHON_LABEL_ADDITION,WHAT_AUTHOR_NOTE,WHAT_ANYONE_NOTE triageOpsClass;
    class WORKFLOW_READY_FOR_REVIEW_LABEL,AUTOMATED_REVIEWER_REQUEST_GENERIC triageOpsClass;
```

### Merge Request Coaches

[Merge Request Coaches](/job-families/expert/merge-request-coach/) are available to help contributors with their MRs. This includes: 
- Identifying reviewers for the MR.
- Answering questions from contributors.
- Educating contributors on the [contribution acceptance criteria](https://docs.gitlab.com/ee/development/contributing/merge_request_workflow.html#contribution-acceptance-criteria).
- Or completing the MR if the contributor is unresponsive or unable to complete.
  - In that case, the `coach will finish` label will be added to the MR and the coach will either directly push new commits to the MR, or re-create a new MR with the original changes. 
  - Contributors can mention the coaches in their MRs by typing `@gitlab-org/coaches`.

Merge Request Coaches can be found in: 
- [List of current Merge Request Coaches](/handbook/marketing/community-relations/code-contributor-program/resources/merge-request-coach-lifecycle.html#current-merge-request-coaches)
- The [team page](/company/team/) by selecting `Merge Request Coach` in the department filter.

There is also the `#mr-coaching` channel in GitLab Slack if GitLab team members have any questions related to community contributions.

More information on Merge Request Coaches (including how to become a Merge Request Coach) can be found in the [MR coach lifecycle page](/handbook/marketing/community-relations/code-contributor-program/resources/merge-request-coach-lifecycle.html).

### Creating educational materials

1. [Gitpod with GDK - Introduction (video)](https://www.youtube.com/watch?v=OzgGP5tT4bo)
1. [Gitpod with GDK - Setup  (video)](https://www.youtube.com/watch?v=6VNm36wdXnI)

### DCO and CLA Guidance

All external contributions to GitLab are subject to the [GitLab DCO or CLA](/community/contribute/dco-cla/), depending on where the contribution is made and on whose behalf.

Instructions for corporate contributors to enter into an overarching Corporate CLA covering all contributions made on their behalf are set out on the [DCO-CLA page](/community/contribute/dco-cla/#need-a-corporate-cla-covering-all-contributors-on-behalf-of-your-organization). 

## Recognizing contributors 

We work with the Community Relations team, to [recognize contributors](/handbook/marketing/community-relations/code-contributor-program/#recognition-for-contributors) 

A nomination process is also available to [nominate a contributor](handbook/marketing/community-relations/code-contributor-program/community-appreciation/)

## Contributor Efficiency Working group

There's a working group with members from Quality and Community Relations that
aims to streamline and improve contributor efficiency. It implements key
business iterations that results in substantial and sustained increases to
community contributors & contributions.

- Handbook page - <https://about.gitlab.com/company/team/structure/working-groups/contribution-efficiency/>
- Agenda - <https://docs.google.com/document/d/1AOgqaslnq-WI1ICSZ1NzSnALf1Va4D5qAD191icAoSI/edit#>

## Code Contributor User Journey

The code contributor user journey is documented in the handbook - [User Journey](./user-journey.html)
