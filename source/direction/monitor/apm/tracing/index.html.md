---
layout: markdown_page
title: "Category Direction - Tracing"
description: "Tracing add visibility into the health of an application end-to-end by measuring the paths of requests. This is GitLab’s direction on where we are headed with Tracing."
canonical_path: "/direction/monitor/apm/tracing/"
---

- TOC
{:toc}

## Overview

## Background 

Tracing here is differentiated from the traditional low-level profiling of application code. We are referring to tracing in the context of observability. 

Tracing, commonly also referred to as distributed tracing, is typically focused on the application layer and provides limited visibility into the health of the underlying infrastructure. There are two important concepts with tracing:

*  **Trace** - represents an application processing a request (e.g., verifying login credential). A trace is typically constructed of multiple Spans.
*  **Span** - represents a single unit of work from start to finish. In microservices environments, spans within a trace are typically “distributed” across multiple services.

With tracing, it is possible to gain in-depth insight into the app across application layers by measuring the execution time of a user journey. Tracing information can be used to troubleshoot an application by measuring different metrics of Traces and Spans.

## Current status

Gitlab integrates with [Jaeger](https://www.jaegertracing.io/) - an open-source, end-to-end distributed tracing system tool used for monitoring and troubleshooting microservices-based distributed systems. If you are using Jaeger it is possible to view its UI within Gitlab.  

We plan to deprecate GitLab’s current tracing solution in 15.0. We plan to introduce tracing in Opstrace after the initial integration between GitLab and Opstrace.

## What's Next & Why

GitLab’s current tracing capabilities are limited and offer little utility to users. 

With the [acquisition of Opstrace](link), we intend to add tracing as part of GitLab’s observability solution. Currently, we are focused on the product integration between GitLab and Opstrace and will not immediately add tracing to Opstrace.
